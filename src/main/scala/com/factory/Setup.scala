package com.factory

import android.content.Context
import com.factory.models.Models.{Component, Construction, Recipe}

import scala.io.Source
import spray.json._
import utils.Utils.PipelineContainer

object Setup {
  def construct(target: String, outputRate: Double)(implicit recipes: Map[String, Recipe]): Construction = {
    val targetRecipe = recipes(target)
    collectIngredients(targetRecipe, outputRate.max(targetRecipe.productionRate))
  }

  private def collectIngredients(recipe: Recipe, requiredRate: Double)
                                (implicit recipes: Map[String, Recipe]): Construction = {
    val multiplier = requiredRate / recipe.productionRate
    val componentList = Map(recipe.name ->
      Component(recipe, requiredRate))
    var construction = Construction(recipe.name, requiredRate, List.empty, componentList, recipe.machine)

    recipe.ingredients.foreach { ingredient =>
      recipes.get(ingredient.name) match {
        case Some(ingRecipe) =>
          val ingList = collectIngredients(ingRecipe, ingredient.requiredRate * multiplier)
          construction =
            construction.addComponentFactory(ingRecipe, ingredient.requiredRate * multiplier, ingList.factory)
          construction = construction.addComponents(ingList.components)
        case None =>
          construction =
            construction.addComponentFactory(ingredient.name, ingredient.requiredRate * multiplier, Seq.empty)
      }
    }

    construction
  }

  def readRecipesFile(implicit ctx: Context): Map[String, Recipe] = {
    val src = TR.raw.recipes.value |> Source.fromInputStream
    src.mkString.parseJson.asJsObject.fields.map {
      case (name, recipe) => name -> recipe.convertTo[Recipe]
    }
  }
}
