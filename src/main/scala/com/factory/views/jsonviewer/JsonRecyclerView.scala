package com.factory.views.jsonviewer

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.factory.utils.Constants.JsonViewerConstants
import com.factory.business.jsonviewer.JsonViewerAdapter

class JsonRecyclerView(val context: Context, val attrs: Option[AttributeSet] = None, val defStyle: Int = 0)
  extends RecyclerView(context, attrs.orNull, defStyle) {

  def this(context: Context, attrs: AttributeSet) {
    this(context, Option(attrs))
  }

  initView()
  private var mJsonAdapter: JsonViewerAdapter = _

  private def initView(): Unit = setLayoutManager(new LinearLayoutManager(getContext))

  def bindJson(jsonStr: String): Unit = {
    mJsonAdapter = new JsonViewerAdapter(jsonStr)
    setAdapter(mJsonAdapter)
  }

  def setTextSize(sizeDP: Float): Unit = {
    val limitedSize = sizeDP.max(10).min(30)
    if (JsonViewerConstants.TEXT_SIZE_DP != limitedSize) {
      JsonViewerConstants.TEXT_SIZE_DP = limitedSize
      if (mJsonAdapter != null) updateAll(limitedSize)
    }
  }

  def setScaleEnable(enable: Boolean): Unit =
    if (enable) addOnItemTouchListener(touchListener)
    else removeOnItemTouchListener(touchListener)

  def updateAll(textSize: Float): Unit = {
    Option(getLayoutManager) foreach { manager =>
      (0 until manager.getChildCount)
        .map(manager.getChildAt)
        .foreach(loop(_, textSize))
    }
  }

  private def loop(view: View, textSize: Float): Unit = view match {
    case group: JsonItemView =>
      group.setTextSize(textSize)
      (0 until group.getChildCount)
        .map(group.getChildAt)
        .foreach(loop(_, textSize))
    case _ =>
  }

  private[views] var mode = 0
  private[views] var oldDist = .0

  private def zoom(f: Float): Unit = setTextSize(JsonViewerConstants.TEXT_SIZE_DP * f)

  private def spacing(event: MotionEvent) = {
    val x = event.getX(0) - event.getX(1)
    val y = event.getY(0) - event.getY(1)
    Math.sqrt(x * x + y * y).toFloat
  }

  final private val touchListener = new RecyclerView.OnItemTouchListener() {
    override def onInterceptTouchEvent(rv: RecyclerView, event: MotionEvent): Boolean = {
      event.getAction & event.getActionMasked match {
        case MotionEvent.ACTION_DOWN =>
          mode = 1
        case MotionEvent.ACTION_UP =>
          mode = 0
        case MotionEvent.ACTION_POINTER_UP =>
          mode -= 1
        case MotionEvent.ACTION_POINTER_DOWN =>
          oldDist = spacing(event)
          mode += 1
        case MotionEvent.ACTION_MOVE =>
          if (mode >= 2) {
            val newDist = spacing(event)
            if (Math.abs(newDist - oldDist) > 0.5f) {
              zoom((newDist / oldDist).toFloat)
              oldDist = newDist
            }
          }
        case _ =>
      }
      false
    }

    override def onTouchEvent(rv: RecyclerView, event: MotionEvent): Unit = {}

    override def onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean): Unit = {}
  }
}