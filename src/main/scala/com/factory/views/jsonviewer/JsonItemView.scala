package com.factory.views.jsonviewer

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.View._
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.LinearLayout._
import android.widget.TextView
import com.factory.{R, TR, TypedFindView}
import com.factory.utils.Constants.JsonViewerConstants

object JsonItemView {
  var TEXT_SIZE_DP = 12
}

class JsonItemView(val mContext: Context, val attrs: Option[AttributeSet] = None, val defStyleAttr: Int = 0)
  extends LinearLayout(mContext, attrs.orNull, defStyleAttr) with TypedFindView {
  implicit val mCtx: Context = mContext

  initView()
  private var mTvLeft: TextView = _
  private var mTvRight: TextView = _
  private var mIvIcon: ImageView = _

  private def initView(): Unit = {
    setOrientation(VERTICAL)
    LayoutInflater.from(mContext).inflate(R.layout.jsonviewer_layout_item_view, this, true)

    mTvLeft = findView(TR.tv_left)
    mTvRight = findView(TR.tv_right)
    mIvIcon = findView(TR.iv_icon)
  }

  def setTextSize(textSizeDp: Float): Unit = {
    val limitedSize = textSizeDp.max(12).min(30)
    JsonItemView.TEXT_SIZE_DP = limitedSize.toInt

    mTvLeft.setTextSize(JsonItemView.TEXT_SIZE_DP)
    mTvRight.setTextSize(JsonItemView.TEXT_SIZE_DP)
    mTvRight.setTextColor(JsonViewerConstants.BRACES_COLOR)

    // align the expand/collapse icon to the text
    val textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, JsonItemView.TEXT_SIZE_DP, getResources.getDisplayMetrics).toInt
    val layoutParams = mIvIcon.getLayoutParams.asInstanceOf[LinearLayout.LayoutParams]
    layoutParams.height = textSize
    layoutParams.width = textSize
    layoutParams.topMargin = textSize / 5
    mIvIcon.setLayoutParams(layoutParams)
  }

  def setRightColor(color: Int): Unit = mTvRight.setTextColor(color)

  def hideLeft(): Unit = mTvLeft.setVisibility(GONE)

  def showLeft(text: CharSequence): Unit = {
    mTvLeft.setVisibility(VISIBLE)
    mTvLeft.setText(text)
  }

  def hideRight(): Unit = mTvRight.setVisibility(GONE)

  def showRight(text: CharSequence): Unit = {
    mTvRight.setVisibility(VISIBLE)
    mTvRight.setText(text)
  }

  def getRightText: CharSequence = mTvRight.getText

  def hideIcon(): Unit = mIvIcon.setVisibility(GONE)

  def showIcon(isPlus: Boolean): Unit = {
    mIvIcon.setVisibility(VISIBLE)
    mIvIcon.setImageDrawable(
      if (isPlus) TR.drawable.jsonviewer_plus.value
      else TR.drawable.jsonviewer_minus.value
    )
    mIvIcon.setContentDescription(
      if (isPlus) TR.string.jsonViewer_icon_plus.value
      else TR.string.jsonViewer_icon_minus.value
    )
  }

  def setIconClickListener(listener: View.OnClickListener): Unit = mIvIcon.setOnClickListener(listener)

  def addViewNoInvalidate(child: View): Boolean = {
    val params =
      Option(child.getLayoutParams)
        .orElse(Option(generateDefaultLayoutParams))
        .getOrElse(throw new IllegalArgumentException("generateDefaultLayoutParams() cannot return null"))

    addViewInLayout(child, -1, params)
  }
}