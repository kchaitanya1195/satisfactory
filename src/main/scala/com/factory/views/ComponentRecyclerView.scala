package com.factory.views

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.{LinearLayoutManager, RecyclerView}
import com.factory.business.{ComponentClickListener, ComponentListAdapter}
import com.factory.models.Models.Component

class ComponentRecyclerView(val mContext: Context, attrs: AttributeSet) extends RecyclerView(mContext, attrs) {
  def bindData(components: List[Component], listener: Option[ComponentClickListener] = None): Unit = {
    setLayoutManager(new LinearLayoutManager(mContext))
    setAdapter(new ComponentListAdapter(components, listener))
  }
}
