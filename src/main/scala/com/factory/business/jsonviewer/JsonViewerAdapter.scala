package com.factory.business.jsonviewer

import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.factory.utils.Utils
import com.factory.views.jsonviewer.JsonItemView
import com.factory.utils.Constants.JsonViewerConstants._
import com.factory.business.jsonviewer.JsonViewerAdapter.JsonItemViewHolder
import spray.json._
import spray.json.DefaultJsonProtocol._

class JsonViewerAdapter(mJson: JsValue) extends RecyclerView.Adapter[JsonItemViewHolder] {

  def this(jsonStr: String) {
    this(jsonStr.parseJson)
  }

  override def onCreateViewHolder(parent: ViewGroup, viewType: Int): JsonItemViewHolder =
    new JsonItemViewHolder(new JsonItemView(parent.getContext))

  override def onBindViewHolder(holder: JsonItemViewHolder, position: Int): Unit = {
    val itemView = holder.jsonItemView
    itemView.setTextSize(TEXT_SIZE_DP)
    itemView.setRightColor(BRACES_COLOR)

    mJson match {
      case JsObject(fields) =>
        if (position == 0) {
          itemView.hideLeft()
          itemView.hideIcon()
          itemView.showRight("{")
        } else if (position == getItemCount - 1) {
          itemView.hideLeft()
          itemView.hideIcon()
          itemView.showRight("}")
        } else if (fields.nonEmpty) {
          val (key, value) = fields.toSeq(position - 1) // Traversal key
          // The last group, no comma at the end
          handleJsonObject(key, value, itemView, position < getItemCount - 2, 1)
        }

      case JsArray(elements) =>
        if (position == 0) {
          itemView.hideLeft()
          itemView.hideIcon()
          itemView.showRight("[")
        } else if (position == getItemCount - 1) {
          itemView.hideLeft()
          itemView.hideIcon()
          itemView.showRight("]")
        } else {
          val value = elements(position - 1) // Traverse the array
          // Last group, no comma at the end
          handleJsonArray(value, itemView, position < getItemCount - 2, 1)
        }

      case _ =>
    }
  }

  override def getItemCount: Int = {
    mJson match {
      case JsObject(fields) => fields.size + 2
      case JsArray(elements) => elements.size + 2
      case _ => 2
    }
  }

  /**
   * Handle the case where the value of the parent is JsonObject, and the value has a key
   */
  private def handleJsonObject(key: String, value: JsValue, itemView: JsonItemView, appendComma: Boolean, hierarchy: Int): Unit = {
    val keyBuilder = new SpannableStringBuilder(Utils.getHierarchyStr(hierarchy))
    keyBuilder.append("\"").append(key).append("\"").append(":")
    keyBuilder.setSpan(new ForegroundColorSpan(KEY_COLOR), 0, keyBuilder.length - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    keyBuilder.setSpan(new ForegroundColorSpan(BRACES_COLOR), keyBuilder.length - 1, keyBuilder.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

    itemView.showLeft(keyBuilder)
    handleValue(value, itemView, appendComma, hierarchy)
  }

  /**
   * Handle the case where the value is JsonArray, and the value has no key
   *
   * @param appendComma Do you need a comma at the end (the last set of values ​​does not need a comma)
   * @param hierarchy   indentation level
   */
  private def handleJsonArray(value: JsValue, itemView: JsonItemView, appendComma: Boolean, hierarchy: Int): Unit = {
    itemView.showLeft(new SpannableStringBuilder(Utils.getHierarchyStr(hierarchy)))
    handleValue(value, itemView, appendComma, hierarchy)
  }

  /**
   * @param appendComma Do you need a comma at the end (the last set of key:value does not need a comma)
   * @param hierarchy   indentation level
   */
  private def handleValue(value: JsValue, itemView: JsonItemView, appendComma: Boolean, hierarchy: Int): Unit = {
    val valueBuilder = new SpannableStringBuilder
    value match {
      case JsNumber(num) =>
        valueBuilder.append(num.toString)
        valueBuilder.setSpan(new ForegroundColorSpan(NUMBER_COLOR), 0, valueBuilder.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

      case JsBoolean(bool) =>
        valueBuilder.append(bool.toString)
        valueBuilder.setSpan(new ForegroundColorSpan(BOOLEAN_COLOR), 0, valueBuilder.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

      case _: JsObject =>
        itemView.showIcon(true)
        valueBuilder.append("{...}")
        valueBuilder.setSpan(new ForegroundColorSpan(BRACES_COLOR), 0, valueBuilder.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        itemView.setIconClickListener(new JsonItemClickListener(value, itemView, appendComma, hierarchy + 1))

      case JsArray(elements) =>
        itemView.showIcon(true)
        valueBuilder.append("[").append(elements.length.toString).append("]")
        val len = valueBuilder.length
        valueBuilder.setSpan(new ForegroundColorSpan(BRACES_COLOR), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        valueBuilder.setSpan(new ForegroundColorSpan(NUMBER_COLOR), 1, len - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        valueBuilder.setSpan(new ForegroundColorSpan(BRACES_COLOR), len - 1, len, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

        itemView.setIconClickListener(new JsonItemClickListener(value, itemView, appendComma, hierarchy + 1))

      case JsString(str) =>
        itemView.hideIcon()
        valueBuilder.append("\"").append(str).append("\"")
        valueBuilder.setSpan(new ForegroundColorSpan(TEXT_COLOR), 0, valueBuilder.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

      case _ => if (valueBuilder.length == 0) {
        itemView.hideIcon()
        valueBuilder.append("null")
        valueBuilder.setSpan(new ForegroundColorSpan(NULL_COLOR), 0, valueBuilder.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
      }
    }
    if (appendComma) valueBuilder.append(",")
    itemView.showRight(valueBuilder)
  }

  private[business] class JsonItemClickListener(val value: JsValue, val itemView: JsonItemView, val appendComma: Boolean,
                                                val hierarchy: Int) extends View.OnClickListener {
    private var isCollapsed = false
    private val isJsonArray = value.isInstanceOf[JsArray]

    override def onClick(view: View): Unit = if (itemView.getChildCount == 1) { // Expanding
      isCollapsed = false
      itemView.showIcon(false)
      itemView.setTag(itemView.getRightText)
      itemView.showRight(
        if (isJsonArray) "["
        else "{"
      )
      val array = value match {
        case JsObject(fields) => fields.keys.map(_.toJson).toSeq
        case JsArray(elements) => elements
        case _ => Seq.empty
      }

      array.zipWithIndex.foreach { case (childValue, i) =>
        val childItemView = new JsonItemView(itemView.getContext)
        childItemView.setTextSize(TEXT_SIZE_DP)
        childItemView.setRightColor(BRACES_COLOR)

        if (isJsonArray)
          handleJsonArray(childValue, childItemView, i < array.length - 1, hierarchy)
        else
          handleJsonObject(
            childValue.convertTo[String],
            value.asJsObject.fields(childValue.convertTo[String]),
            childItemView,
            i < array.length - 1,
            hierarchy
          )

        itemView.addViewNoInvalidate(childItemView)
      }

      val childItemView = new JsonItemView(itemView.getContext)
      childItemView.setTextSize(TEXT_SIZE_DP)
      childItemView.setRightColor(BRACES_COLOR)

      val builder = new StringBuilder(Utils.getHierarchyStr(hierarchy - 1))
      builder.append(
        if (isJsonArray) "]"
        else "}"
      ).append(
        if (appendComma) ","
        else ""
      )
      childItemView.showRight(builder)
      itemView.addViewNoInvalidate(childItemView)
      itemView.requestLayout()
      itemView.invalidate()
    } else { // Collapsing
      val temp = itemView.getRightText
      itemView.showRight(itemView.getTag.asInstanceOf[CharSequence])
      itemView.setTag(temp)
      itemView.showIcon(!isCollapsed)
      (1 until itemView.getChildCount)
        .map(itemView.getChildAt)
        .foreach(_.setVisibility(
          if (isCollapsed) View.VISIBLE
          else View.GONE
        ))

      isCollapsed = !isCollapsed
    }
  }
}

object JsonViewerAdapter {
  private[business] class JsonItemViewHolder(val jsonItemView: JsonItemView) extends RecyclerView.ViewHolder(jsonItemView) {
    setIsRecyclable(false)
  }
}
