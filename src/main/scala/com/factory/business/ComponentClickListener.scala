package com.factory.business

trait ComponentClickListener {
  def onClick(name: String, requiredRate: Double): Unit
}
