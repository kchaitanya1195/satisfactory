package com.factory.business

import android.graphics.Color
import android.util.Log
import android.view.{LayoutInflater, View, ViewGroup}
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.factory.TypedResource.TypedView
import com.factory.{R, TR}
import com.factory.business.ComponentListAdapter.ComponentViewHolder
import com.factory.models.Models.{Component, Recipe}

class ComponentListAdapter(components: List[Component], listener: Option[ComponentClickListener] = None)
  extends RecyclerView.Adapter[ComponentViewHolder] {
  override def onCreateViewHolder(viewGroup: ViewGroup, i: Int): ComponentViewHolder = {
    Log.d("Adapter", s"create VH $i: $listener")
    val view = LayoutInflater.from(viewGroup.getContext).inflate(R.layout.component, viewGroup, false)
    new ComponentViewHolder(view, listener)
  }

  override def onBindViewHolder(vh: ComponentViewHolder, i: Int): Unit = {
    Log.d("Adapter", s"bind VH $i: ${components(i).recipe.name}")
    val component = components(i)

    if (i%2 == 0)
      vh.view.setBackgroundColor(Color.DKGRAY)
    else
      vh.view.setBackgroundColor(Color.TRANSPARENT)

    vh.bindData(component)
  }

  override def getItemCount: Int = components.size
}

object ComponentListAdapter {
  class ComponentViewHolder(val view: View, val listener: Option[ComponentClickListener] = None)
    extends RecyclerView.ViewHolder(view) with View.OnClickListener {
    var recipe: Option[Recipe] = None
    var requiredRate: Option[Double] = None

    val name: TextView = view.findView(TR.compName)
    val machine: TextView = view.findView(TR.compMachine)
    val rate: TextView = view.findView(TR.compRate)
    val count: TextView = view.findView(TR.compCount)

    view.setOnClickListener(this)

    def bindData(component: Component): Unit = {
      this.recipe = Option(component.recipe)
      this.requiredRate = Option(component.requiredRate)

      name.setText(component.recipe.name)
      machine.setText(component.recipe.machine)
      rate.setText(f"${component.requiredRate}%.2f/${component.recipe.productionRate}%.2f")
      count.setText(component.productionCount.toString)
    }

    override def onClick(view: View): Unit = {
      Log.d("AdapterVH", s"Clicked for $recipe: $requiredRate")
      for {
        l <- listener
        r <- recipe
        rr <- requiredRate
      } yield l.onClick(r.name, rr)
    }
  }
}
