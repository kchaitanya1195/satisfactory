package com.factory

import android.os.Bundle
import android.text.{Editable, TextWatcher}
import android.util.Log
import android.view.View
import android.view.View.{GONE, VISIBLE}
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.{AdapterView, ArrayAdapter, Button, EditText, TextView}
import androidx.appcompat.app.AppCompatActivity
import com.factory.business.ComponentClickListener
import com.factory.utils.Constants.machineOrder
import com.factory.models.Models
import com.factory.models.Models.Component
import com.factory.views.ComponentRecyclerView
//import com.factory.views.jsonviewer.JsonRecyclerView
import com.toptoche.searchablespinnerlibrary.SearchableSpinner
//import spray.json._

import scala.collection.JavaConverters._
import scala.collection.mutable

class MainActivity extends AppCompatActivity with TypedFindView with ComponentClickListener {
    // allows accessing `.value` on TR.resource.constants
    implicit val context: MainActivity = this

    var recipes: Map[String, Models.Recipe] = _
    var recipeStack: mutable.Stack[(String, Double)] = _

    var recipeSpinner: SearchableSpinner = _
    // var consoleView: JsonRecyclerView = _
    // User enters output rate into this text box
    var outputView: EditText = _

    // Main info
    var recipeName: TextView = _
    var machine: TextView = _

    // Button bar
    var componentsBtn: Button = _
    var factoryBtn: Button = _

    // Construction info
    var components: ComponentRecyclerView = _
    var factory: ComponentRecyclerView = _

    override def onCreate(savedInstanceState: Bundle): Unit = {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
        if(this.getSupportActionBar != null)
            this.getSupportActionBar.hide()

        recipes = Setup.readRecipesFile(this)
        recipeStack = new mutable.Stack()
        val adapter = new ArrayAdapter[String](
            this, R.layout.support_simple_spinner_dropdown_item, recipes.keys.toSeq.asJava)

        recipeSpinner = findView(TR.recipes)
        recipeSpinner.setAdapter(adapter)
        recipeSpinner.setTitle("Select Item")
        recipeSpinner.setOnItemSelectedListener(new OnItemSelectedListener {
            override def onItemSelected(adapterView: AdapterView[_], view: View, i: Int, l: Long): Unit =
                construct()

            override def onNothingSelected(adapterView: AdapterView[_]): Unit = {}
        })

//        consoleView = findView(TR.console)
//        consoleView.setTextSize(20)
//        consoleView.setScaleEnable(true)

        outputView = findView(TR.output)
        outputView.addTextChangedListener(new TextWatcher {
            override def beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int): Unit = {}

            override def onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int): Unit = {
                if (charSequence.toString.nonEmpty && charSequence.toString.toDouble > 0)
                    construct()
            }

            override def afterTextChanged(editable: Editable): Unit = {}
        })

        // Main info
        recipeName = findView(TR.recipeName)
        machine = findView(TR.machine)

        // Button bar
        componentsBtn = findView(TR.btnComponents)
        componentsBtn.setOnClickListener(new View.OnClickListener {
            override def onClick(view: View): Unit = {
                components.setVisibility(VISIBLE)
                factory.setVisibility(GONE)
            }
        })
        factoryBtn = findView(TR.btnFactory)
        factoryBtn.setOnClickListener(new View.OnClickListener {
            override def onClick(view: View): Unit = {
                components.setVisibility(GONE)
                factory.setVisibility(VISIBLE)
            }
        })

        // Construction info
        components = findView(TR.components)
        components.setVisibility(VISIBLE)

        factory = findView(TR.factory)
        factory.setOnClickListener(new View.OnClickListener {
            override def onClick(view: View): Unit = {
                Log.d("Main", s"Clicked on factory: ${factory.getChildAdapterPosition(view)}")
            }
        })
        factory.setVisibility(GONE)
    }

    def construct(): Unit = {
        val selected = recipeSpinner.getSelectedItem.toString
        val outputRate = outputView.getText.toString.toDouble

        recipeStack.clear()
        recipeStack.push(selected -> outputRate)
        construct(selected, outputRate)
    }

    def construct(name: String, outputRate: Double): Unit = {
        val construction = Setup.construct(name, outputRate)(recipes)
        //consoleView.bindJson(construction.toJson.prettyPrint)

        recipeName.setText(f"${construction.name} (${construction.productionRate}%.2f)")
        machine.setText(construction.machine)

        val componentsList = construction.components.values.toList
        components.bindData(componentsList.sortBy(c => machineOrder(c.recipe.machine)))

        val factoryComponents = construction.factory.map(f => Component(f.recipe, f.requiredRate))
        factory.bindData(factoryComponents.sortBy(c => machineOrder(c.recipe.machine)), Option(this))
    }

    override def onClick(name: String, requiredRate: Double): Unit = {
        recipeStack.push(name -> requiredRate)
        construct(name, requiredRate)
    }

    override def onBackPressed(): Unit = {
        Log.d("Main", s"Back Pressed $recipeStack")
        if (recipeStack.size <= 1)
            super.onBackPressed()
        else {
            recipeStack.pop()
            val (recipe, rate) = recipeStack.head
            construct(recipe, rate)
        }
    }
}
