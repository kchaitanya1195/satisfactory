package com.factory.utils

import spray.json._

object Utils {
  implicit class PipelineContainer[F](val value: F) {
    def |>[G] (f: F => G): G = f(value)
  }

  def getHierarchyStr(hierarchy: Int): String = "      " * hierarchy

  trait JsonUtils {
    implicit def listWriter[A: JsonWriter]: RootJsonWriter[List[A]] = new RootJsonWriter[List[A]] {
      override def write(list: List[A]): JsValue = JsArray(list.map(_.toJson):_*)
    }
    implicit def mapWriter[A: JsonWriter]: RootJsonWriter[Map[String, A]] = new RootJsonWriter[Map[String, A]] {
      override def write(map: Map[String, A]): JsValue = JsObject(map.map(e => e._1 -> e._2.toJson))
    }
  }
}
