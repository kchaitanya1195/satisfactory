package com.factory.utils

object Constants {
  val NAME = "Name"
  val INPUT = "Input(s)"
  val OUTPUT = "Output(s)"
  val AMOUNT = "Amount"
  val MACHINE = "Machine"
  val DURATION = "Duration"
  val TOOL = "Produced With"
  val ING = "Ingredients"
  val TYPE = "Type"

  val ORE = "Ore"
  val ITEM = "Item"

  val machineOrder = Map(
    "Smelter" -> 2,
    "Foundry" -> 3,
    "Oil Refinery" -> 4,
    "Assembler" -> 6,
    "Manufacturer" -> 7,
    "Miner" -> 1,
    "Constructor" -> 5
  )

  object JsonViewerConstants {
    val KEY_COLOR = 0xFF922799
    val TEXT_COLOR = 0xFF3AB54A
    val NUMBER_COLOR = 0xFF25AAE2
    val BOOLEAN_COLOR = 0xFFF98280
    val NULL_COLOR = 0xFFEF5935
    val BRACES_COLOR = 0xFF4A555F
    var TEXT_SIZE_DP: Float = 12
  }

}
