package com.factory.models

import com.factory.utils.Constants._
import com.factory.utils.Utils.JsonUtils
import spray.json._
import spray.json.DefaultJsonProtocol._

object Models extends JsonUtils{
  case class Ingredient(name: String, amount: Int, operationRate: Double) {
    val requiredRate: Double = operationRate * amount
  }

  case class Recipe(name: String, oType: String, duration: Int, amount: Int, ingredients: Seq[Ingredient],
                    machine: String) {
    val productionRate: Double = 60.0 / duration * amount
  }
  implicit val recipeFmt: RootJsonReader[Recipe] = new RootJsonReader[Recipe] {
    override def read(json: JsValue): Recipe = {
      json.asJsObject.getFields(NAME, TYPE, DURATION, AMOUNT, ING, MACHINE) match {
        case Seq(JsString(name), JsString(oType), JsNumber(duration),
        JsNumber(amount), JsArray(ingredients), JsString(machine)) =>
          val ings = ingredients.map { ing =>
            ing.asJsObject.getFields(NAME, AMOUNT) match {
              case Seq(JsString(name), JsNumber(amount)) =>
                Ingredient(name, amount.toIntExact, 60.0 / duration.toDouble)
              case _ => throw DeserializationException(s"Invalid ingredient: $ing")
            }
          }
          Recipe(name, oType, duration.toIntExact, amount.toIntExact, ings, machine)
        case _ => throw DeserializationException(s"Invalid recipe: $json")
      }
    }
  }

  case class Component(recipe: Recipe, requiredRate: Double) {
    val productionCount: Int = (requiredRate / recipe.productionRate).ceil.toInt

    def addRate(rate: Double): Component = this.copy(requiredRate = requiredRate + rate)
  }
  implicit val componentFmt: RootJsonWriter[Component] = new RootJsonWriter[Component] {
    override def write(obj: Component): JsValue =
      JsObject(
        "name" -> obj.recipe.name.toJson,
        "productionRate" -> obj.recipe.productionRate.toJson,
        "requiredRate" -> obj.requiredRate.toJson,
        "productionCount" -> obj.productionCount.toJson,
        "machine" -> obj.recipe.machine.toJson
      )
  }

  case class Factory(recipe: Recipe, requiredRate: Double, factory: Seq[Factory])
  implicit val factoryFmt: RootJsonWriter[Factory] = new RootJsonWriter[Factory] {
    override def write(obj: Factory): JsValue = {
      val fields = Seq(
        Some(obj.recipe.name -> obj.requiredRate.toJson),
        Some("machine" -> obj.recipe.machine.toJson),
        if (obj.factory.nonEmpty)
          Some("factory" -> JsArray(obj.factory.map(this.write): _*))
        else None
      ).flatten
      JsObject(fields:_*)
    }
  }

  case class Construction(name: String, productionRate: Double, factory: List[Factory],
                          components: Map[String, Component], machine: String) {
    def addComponentFactory(compRecipe: Recipe, requiredRate: Double, factory: Seq[Factory]): Construction =
      this.copy(factory = this.factory :+ Factory(compRecipe, requiredRate, factory))

    def addComponents(components: Map[String, Component]): Construction = {
      val newComponents =
        components.foldLeft(this.components) { case (comps, (compName, component)) =>
          val newComp =
            comps.get(compName)
              .map(_.addRate(component.requiredRate))
              .getOrElse(component)

          comps + (compName -> newComp)
        }

      this.copy(components = newComponents)
    }
  }
  implicit val constructionFmt: RootJsonWriter[Construction] = new RootJsonWriter[Construction] {
    override def write(obj: Construction): JsValue =
      JsObject(
        "factory" -> obj.factory.toJson,
        "components" -> obj.components.toJson
      )
  }
}
