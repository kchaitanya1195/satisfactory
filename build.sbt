
enablePlugins(AndroidApp)
scalaVersion := "2.11.5"

platformTarget in Android := "android-29"
minSdkVersion in Android := "26"

proguardOptions in Android ++= Seq(
  "-keep class scala.Dynamic { *; }",
  //"-keep class scala.collection.*",
  "-dontwarn scala.**",
  "-dontwarn"
)

resolvers ++= Seq(
  "Google's Maven repository" at "https://dl.google.com/dl/android/maven2/",
  "Spring Plugins" at "https://repo.spring.io/plugins-release/"
)

libraryDependencies ++= Seq(
  "androidx.appcompat" % "appcompat" % "1.2.0" % "runtime",
  "androidx.annotation" % "annotation" % "1.1.0",
  "androidx.lifecycle" % "lifecycle-common" % "2.2.0",
  "androidx.recyclerview" % "recyclerview" % "1.1.0" % "runtime",
//  "androidx.constraintlayout" % "constraintlayout-solver" % "2.0.4" % "runtime",
//  "androidx.constraintlayout" % "constraintlayout" % "2.0.4" % "runtime",
  "io.spray" %%  "spray-json" % "1.3.5",
  "com.toptoche.searchablespinner" % "searchablespinnerlibrary" % "1.3.1"
)
